import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Yoshinoya {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea textArea1;

    void bottan (String foodname){
        int confirmation = JOptionPane.showConfirmDialog(null ,"Would you like to order"+foodname+"?" ,
                "Order Confirmation",JOptionPane.YES_NO_OPTION );

        if(confirmation == 0 ){

            //ここにダイアログをもう一つ開く→OK→テキスト変更
            JOptionPane.showMessageDialog(null , "Order for " + foodname + " received .");

            textArea1.setText("Order for " + foodname + " received.");

        }
    }

    public Yoshinoya() {

        tempuraButton.addActionListener(new ActionListener() {
           @Override
            public void actionPerformed(ActionEvent e) {

                bottan("Tempura");

            }

        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                bottan("Ramen");


            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                bottan("Udon");


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Yoshinoya");
        frame.setContentPane(new Yoshinoya().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
